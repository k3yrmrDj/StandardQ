const
std::unordered_set <std::wstring> __blacklist = {
  L"steamwebhelper.exe",
  L"streaming_client.exe",
  L"Seam.exe",
  L"html5app_steam.exe",
  L"wow_helper.exe",
  L"uninstall.exe",
  
  L"writeminidump.exe",
  L"crashreporter.exe",
  L"supporttool.exe",
  L"crashsender1400.exe",
  L"firaxisbugreporter.exe",
  
  L"dxsetup.exe",
  L"setup.exe",
  L"vc_redist.x64.exe",
  L"vc_redist.x86.exe"  
  L"vc2010redist_x64.exe",
  L"vc2010redist_x86.exe",
  L"vcredist_x64.exe",
  L"vcredist_x86.exe",
  L"ndp451-kb2872776-x86-x64-allos-enu.exe",
  L"dotnetfx35.exe",
  L"dotnetfx35client.exe",
  L"dotnetfx40_full_x86_x64.exe",
  L"dotnetfx40_client_x86_x64.exe",
  L"oalinst.exe",
  L"easyanticheat_setup.exe",
  L"uplayinstaller.exe",
  
  L"x64launcher.exe",
  L"x86launcher.exe",
  L"launcher.exe",
  L"ffx&x-2_launcher.exe",
  L"fallout4launcher.exe",
  L"skyrimselauncher.exe",
  L"modlauncher.exe",
  L"akibauu_config.exe",
  L"obduction.exe",
  L"grandia2launcher.exe",
  L"ffxiii2launcher.exe",
  L"bethesda.net_launcher.exe",
  L"ubisoftgamelauncher.exe",
  L"ubisoftgamelauncher64.exe",
  L"splashscreen.exe",
  L"gamelaunchercefchildprocess.exe",
  L"launchpad.exe",
  L"cnnlauncher.exe",
  L"ff9_launcher.exe",
  L"a17config.exe",
  L"a18config.exe", // Atelier Firis
  L"dplauncher.exe",
  L"zeroescape-launcher.exe",
  L"gtavlauncher.exe",
  L"gtavlanguageselect.exe",
  L"nioh_launcher.exe",
  L"rottlauncher.exe",
  L"configtool.exe",

  L"coherentui_host.exe",
  L"vhui64.exe",
  L"vhui.exe",
  L"activationui.exe",
  L"zossteamstarter.exe",
  L"eac.exe",
  L"devenv.exe",
  L"vcpkgsrv.exe",
  
  L"gameserver.exe",// Sacred   game server
  L"s2gs.exe",      // Sacred 2 game server
  
  L"launchtm.exe",
  
  L"ds3t.exe",
  L"tzt.exe"
};